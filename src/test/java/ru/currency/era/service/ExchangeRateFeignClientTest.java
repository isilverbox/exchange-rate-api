package ru.currency.era.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.currency.era.dto.currency.Currency;
import ru.currency.era.mock.ExchangeRateMocks;
import ru.currency.era.mock.WireMockConfig;

import java.io.IOException;
import java.net.URI;


@SpringBootTest
@ActiveProfiles("test")
@EnableConfigurationProperties
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {WireMockConfig.class})
public class ExchangeRateFeignClientTest {

	@Autowired
	private WireMockServer wireMockServer;

	@Autowired
	private ExchangeRateFeignClient exchangeRateFeignClient;

	/*@Before
	public void setUp() throws Exception {

	}*/

	@Test
	public void rates() throws IOException {
		ExchangeRateMocks.setupMockExchangeRateResponse(wireMockServer, 200);
		Currency later = exchangeRateFeignClient.rates(URI.create(wireMockServer.baseUrl()));
		Assertions.assertNotNull(later);
		Assertions.assertNotNull(later.getRates().getRub());
	}
}