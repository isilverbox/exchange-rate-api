package ru.currency.era.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import feign.FeignException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.currency.era.dto.images.ImageHolder;
import ru.currency.era.mock.ImageMocks;
import ru.currency.era.mock.WireMockConfig;

import java.io.IOException;
import java.net.URI;

@SpringBootTest
@ActiveProfiles("test")
@EnableConfigurationProperties
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {WireMockConfig.class})
class ImageResourceFeignClientTest {
	private static final Logger logger = LogManager.getLogger(ImageResourceFeignClientTest.class);

	@Autowired
	private WireMockServer mockPictureService;

	@Autowired
	private ImageResourceFeignClient imageResourceFeignClient;

	@Test
	public void getPictureSuccessfully() throws Exception {
		ImageMocks.setupMockPicturesResponse(mockPictureService, 200);

		ImageHolder holder = imageResourceFeignClient.getImage(URI.create(mockPictureService.baseUrl()));
		Assertions.assertNotNull(holder);
		Assertions.assertNotNull(holder.getData().get(0).getImages().getOriginal().getUrl());
		Assertions.assertEquals("https://media1.giphy.com/media/ZGH8VtTZMmnwzsYYMf/giphy.gif?cid=743dbb53aum5ovnryqiratw60r1d2n67z0l62d1t2upuafpd&rid=giphy.gif",
				holder.getData().get(0).getImages().getOriginal().getUrl());
	}

	@Test
	public void getPictureError() throws IOException {
		ImageMocks.setupMockPicturesResponse(mockPictureService, 404);

		Assertions.assertThrows(FeignException.class,
				() -> imageResourceFeignClient.getImage(URI.create(mockPictureService.baseUrl())));
	}

}