package ru.currency.era.mock;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.springframework.http.MediaType;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;

public class ExchangeRateMocks {
	public static void setupMockExchangeRateResponse(WireMockServer mockService, int responseCode) throws IOException {
		mockService.stubFor(
				WireMock.get(WireMock.urlEqualTo("/"))
						.willReturn(WireMock.aResponse()
								.withStatus(responseCode)
								.withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
								.withBody(
										StreamUtils.copyToString(
												ImageMocks.class.getClassLoader().getResourceAsStream("response/exchange-rate-response.json"),
												Charset.defaultCharset()))));
	}
}
