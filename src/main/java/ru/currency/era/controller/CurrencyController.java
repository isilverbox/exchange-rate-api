package ru.currency.era.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.currency.era.service.CurrencyService;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("currency")
public class CurrencyController {

	final CurrencyService currencyService;

	public CurrencyController(CurrencyService currencyService) {
		this.currencyService = currencyService;
	}

	@GetMapping("/rate/{name}")
	public void getRate(@PathVariable("name") String name,
						  HttpServletResponse response) throws IOException {

		Optional<byte[]> result = currencyService.compareAndGetImage(name);
		if (result.isPresent()) {
			response.setStatus(200);
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			IOUtils.copy(new ByteArrayInputStream(result.get()), response.getOutputStream());
		} else {
			response.sendError(500);
		}
	}
}



