package ru.currency.era.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "image.service")
public class ImageURLConfiguration {
	private int a;
	private int b;

	private String rich;
	private String broke;

	public String getRandomizedRich() {
		return String.format(rich, getRandomizedInt());
	}

	public String getRandomizedBroke() {
		return String.format(broke, getRandomizedInt());
	}

	private int getRandomizedInt() {
		return a + (int) (Math.random() * b);
	}
}
