package ru.currency.era.configuration;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Setter
@Configuration
@ConfigurationProperties(prefix = "currency.service")
public class CurrencyURLConfiguration {

	private String authKey;

	private String dateFormat;

	private String latest;
	private String historical;

	public String getHistorical(String currency, LocalDate date) {
		return String.format(historical, date.format(DateTimeFormatter.ofPattern(dateFormat)), currency);
	}

	public String getLatest(String currency) {
		return String.format(latest, currency);
	}
}
