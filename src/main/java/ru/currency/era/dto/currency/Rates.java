package ru.currency.era.dto.currency;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@ToString
@Getter
@Setter
public class Rates {

	@JsonProperty("RUB")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private BigDecimal rub;
}
