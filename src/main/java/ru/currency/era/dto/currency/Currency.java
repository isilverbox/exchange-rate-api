package ru.currency.era.dto.currency;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class Currency {

	@JsonProperty("base")
	String base;
	@JsonProperty("rates")
	Rates rates;
}
