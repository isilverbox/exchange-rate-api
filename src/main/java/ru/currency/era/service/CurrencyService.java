package ru.currency.era.service;

import feign.FeignException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import ru.currency.era.configuration.CurrencyURLConfiguration;
import ru.currency.era.configuration.ImageURLConfiguration;
import ru.currency.era.dto.currency.Currency;
import ru.currency.era.dto.images.ImageHolder;

import java.net.URI;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class CurrencyService {
	private static final Logger logger = LogManager.getLogger(CurrencyService.class);

	final CurrencyURLConfiguration currencyURLConfiguration;
	final ImageURLConfiguration imageURLConfiguration;

	final ExchangeRateFeignClient exchangeRateFeignClient;
	final ImageResourceFeignClient imageResourceFeignClient;

	public CurrencyService(CurrencyURLConfiguration currencyURLConfiguration,
						   ImageURLConfiguration imageURLConfiguration,
						   ExchangeRateFeignClient exchangeRateFeignClient,
						   ImageResourceFeignClient imageResourceFeignClient) {
		this.currencyURLConfiguration = currencyURLConfiguration;
		this.exchangeRateFeignClient = exchangeRateFeignClient;
		this.imageResourceFeignClient = imageResourceFeignClient;
		this.imageURLConfiguration = imageURLConfiguration;
	}

	// Compare RUB with currencyName
	public int compareCurrents(String currencyName) throws FeignException, Exception {
		String latestUrl = currencyURLConfiguration.getLatest(currencyName);
		String historicalUrl = currencyURLConfiguration.getHistorical(currencyName, LocalDate.now().minusDays(1));

		Currency latest = exchangeRateFeignClient.rates(URI.create(latestUrl));
		Currency historical = exchangeRateFeignClient.rates(URI.create(historicalUrl));

		logger.info("Historical: {}, Latest: {}", historical, latest);
		return latest.getRates().getRub().compareTo(historical.getRates().getRub());
	}

	public byte[] getImage(int compare) throws FeignException, Exception {
		ImageHolder imageHolder;
		if (compare > 0) {
			imageHolder = imageResourceFeignClient.getImage(
					URI.create(imageURLConfiguration.getRandomizedRich()));

			if (imageHolder != null) {
				return imageResourceFeignClient.getPicture(
						URI.create(imageHolder.getData().get(0).getImages().getOriginal().getUrl()));
			}

		} else if (compare < 0) {
			System.out.println(URI.create(imageURLConfiguration.getRandomizedBroke()));
			imageHolder = imageResourceFeignClient.getImage(URI.create(imageURLConfiguration.getRandomizedBroke()));

			if (imageHolder != null) {

				return imageResourceFeignClient.getPicture(
						URI.create(imageHolder.getData().get(0).getImages().getOriginal().getUrl()));
			}
		}
		return null;
	}

	public Optional<byte[]> compareAndGetImage(String currencyName) {
		try {
			int compared = compareCurrents(currencyName);
			byte[] image = getImage(compared);
			return Optional.ofNullable(image);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}
}
