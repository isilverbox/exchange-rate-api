package ru.currency.era.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import ru.currency.era.dto.images.ImageHolder;

import java.net.URI;

@FeignClient(value = "${image.service.url}", url = "${image.service.url}")
public interface ImageResourceFeignClient {

	@GetMapping("")
	@ExceptionHandler
	ImageHolder getImage(URI uri)throws Exception;

	@GetMapping("")
	@ExceptionHandler
	byte[] getPicture(URI uri) throws Exception;
}
