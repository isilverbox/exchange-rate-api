package ru.currency.era.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import ru.currency.era.dto.currency.Currency;

import java.net.URI;

@FeignClient(value = "${currency.service.url}", url = "${currency.service.url}")
public interface ExchangeRateFeignClient {

	@GetMapping("")
	@ExceptionHandler
	Currency rates(URI uri);
}
