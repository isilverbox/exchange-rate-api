<h1>Exchange Rate Api</h1>
<h3>Build instruction</h3>
Build single jar (need gradle and java 11+)<br>
1. `git clone https://gitlab.com/isilverbox/exchange-rate-api.git`<br>
2. `cd exchange-rate-api`<br>
3. `gradle build`<br><br>

Or Docker pull<br>
1. docker pull isilverbox/exchange-rate-dynamics<br>
<h3>Default listening</h3>
http://localhost:8080/currency/rate/usd<br><br>

To be able to choose a currency change demo key in application.properties `currency.service.authKey`
